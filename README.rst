======================
Cookiecutter PyPackage
======================

.. image:: https://gitlab.com/AdriaanRol/cookiecutter-pypackage-gitlab/badges/master/pipeline.svg
    :target: https://gitlab.com/AdriaanRol/cookiecutter-pypackage-gitlab/pipelines/
    :alt: Build Status

.. image:: https://img.shields.io/pypi/v/cookiecutter-pypackage-gitlab.svg
    :target: https://pypi.org/pypi/cookiecutter-pypackage-gitlab
    :alt: PyPI

.. image:: https://gitlab.com/AdriaanRol/cookiecutter-pypackage-gitlab/badges/master/coverage.svg
    :target: https://gitlab.com/AdriaanRol/cookiecutter-pypackage-gitlab/pipelines/
    :alt: Coverage


.. image:: https://readthedocs.org/projects/cookiecutter-pypackage-gitlab/badge/?version=latest
  :target: https://cookiecutter-pypackage-gitlab.readthedocs.io/en/latest/?badge=latest
  :alt: Documentation Status


Cookiecutter_ template for a Python package edited for Advanced Analytics Team.

* Documentation: https://cookiecutter-pypackage.readthedocs.io/

Features
--------

* Testing setup with ``unittest`` and ``python setup.py test`` or ``pytest``
* Bitbucket_Pipeline: Ready for Bitbucket CI/CD testing
* Tox_ testing: Setup to easily test for Python 3.5, 3.6, 3.7, 3.8
* Sphinx_ docs: Documentation ready for generation with, for example
* bump2version_: Pre-configured version bumping with a single command
* Auto-release to PyPI_ when you push a new tag to master (optional)
* Command line interface using Click (optional)

.. _Cookiecutter: https://github.com/cookiecutter/cookiecutter

Build Status
-------------

Linux:

.. image:: https://img.shields.io/travis/audreyfeldroy/cookiecutter-pypackage.svg
    :target: https://travis-ci.org/audreyfeldroy/cookiecutter-pypackage
    :alt: Linux build status on Travis CI

Quickstart
----------

Install the latest Cookiecutter if you haven't installed it yet (this requires
Cookiecutter 1.4.0 or higher)::

    pip install -U cookiecutter

Generate a Python package project::

    cookiecutter https://will-hu@bitbucket.org/phlds/cookiecutter-pypackage.git

Then:
* (``pip install -r requirements_dev.txt``)

For more details, see the `cookiecutter-pypackage tutorial`_.

.. _`cookiecutter-pypackage tutorial`: https://cookiecutter-pypackage.readthedocs.io/en/latest/tutorial.html


.. _Tox: http://testrun.org/tox/
.. _Sphinx: http://sphinx-doc.org/
.. _Read the Docs: https://readthedocs.io/
.. _`pyup.io`: https://pyup.io/
.. _bump2version: https://github.com/c4urself/bump2version
.. _Punch: https://github.com/lgiordani/punch
.. _Poetry: https://python-poetry.org/
.. _PyPi: https://pypi.python.org/pypi

.. _`Nekroze/cookiecutter-pypackage`: https://github.com/Nekroze/cookiecutter-pypackage
.. _`tony/cookiecutter-pypackage-pythonic`: https://github.com/tony/cookiecutter-pypackage-pythonic
.. _`ardydedase/cookiecutter-pypackage`: https://github.com/ardydedase/cookiecutter-pypackage
.. _`lgiordani/cookiecutter-pypackage`: https://github.com/lgiordani/cookiecutter-pypackage
.. _`briggySmalls/cookiecutter-pypackage`: https://github.com/briggySmalls/cookiecutter-pypackage
.. _github comparison view: https://github.com/tony/cookiecutter-pypackage-pythonic/compare/audreyr:master...master
.. _`network`: https://github.com/audreyr/cookiecutter-pypackage/network
.. _`family tree`: https://github.com/audreyr/cookiecutter-pypackage/network/members
