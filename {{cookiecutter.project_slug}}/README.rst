{% set is_open_source = cookiecutter.open_source_license != 'Not open source' -%}
{% for _ in cookiecutter.project_name %}={% endfor %}
{{ cookiecutter.project_name }}
{% for _ in cookiecutter.project_name %}={% endfor %}


.. image:: {{ cookiecutter.project_url}}/badges/master/pipeline.svg
    :target: {{ cookiecutter.project_url}}/pipelines/
    :alt: Build Status

.. image:: https://img.shields.io/pypi/v/{{ cookiecutter.project_slug }}.svg
    :target: https://pypi.org/pypi/{{ cookiecutter.project_slug }}
    :alt: PyPI

.. image:: {{ cookiecutter.project_url}}/badges/master/coverage.svg
    :target: {{ cookiecutter.project_url}}/pipelines/
    :alt: Coverage


.. image:: https://readthedocs.org/projects/{{ cookiecutter.project_slug | replace("_", "-") }}/badge/?version=latest
        :target: https://{{ cookiecutter.project_slug | replace("_", "-") }}.readthedocs.io/en/latest/?badge=latest
        :alt: Documentation Status


.. image:: https://img.shields.io/badge/License-{{ cookiecutter.open_source_license | replace("-", "--")}}-blue.svg
    :target: {{ cookiecutter.project_url}}/-/blob/master/LICENSE





{{ cookiecutter.project_short_description }}

{% if is_open_source %}
* Free software: {{ cookiecutter.open_source_license }}
* Documentation: https://{{ cookiecutter.project_slug | replace("_", "-") }}.readthedocs.io.
{% endif %}

{% if not is_open_source %}
* Proprietary: {{ cookiecutter.open_source_license }}
* Documentation: https://{{ cookiecutter.project_slug | replace("_", "-") }}.readthedocs.com.
{% endif %}

Features
--------

* TODO

Credits
-------

This package was created with Cookiecutter_ and the `cookiecutter-pypackage-gitlab`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`cookiecutter-pypackage-gitlab`: https://gitlab.com/AdriaanRol/cookiecutter-pypackage-gitlab
