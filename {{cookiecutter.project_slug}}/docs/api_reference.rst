
{{ cookiecutter.project_slug }}
=============

.. automodule:: {{ cookiecutter.project_slug }}
   :members:
